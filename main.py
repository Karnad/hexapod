#! /usr/bin/env  python3

from src.hexapod import Hexapod

if __name__ == "__main__":
    try:
        momo = Hexapod()
        momo.start()

    except KeyboardInterrupt:
        momo.__del__()
