#! /usr/bin/env  python3

def time_string(speed, interval):
    """
    Translate a speed into a SSC32-U time string of a unit move.

    :param speed: The speed of the unit move. The speed varies between 0 and\
    3, with a speed of 1, the unit move will take exactly INTERVAL time.
    :return: The SSC32-U time string in milliseconds.
    """
    return "T" + str(int(interval * 1000 / speed))

def smoothstep(value1, value2, percent):
    """
    Interpolate the coordonates between value1 and value2 according to percent.

    :param value1:  The first coordonate.
    :param value2:  The second coordonate.
    :param percent: The percentage that represent where to interpolate.

    :return:        The interpolated coordonates.
    """
    percent = min(1.0, max(0.0, percent))
    return [i[0] + (i[1] - i[0]) * percent for i in zip(value1, value2)]
